import java.util.Scanner;

/*
 * Pour la question 2 j'ai choisi de supprimer toute les classes héritantes de la classe Alcool et de creer une énumération a la place
 * je n'ai pas réussi a trouver de méthode a modifier dans la question 1 
 * j'ai donc modifié la classe Alcool pour cette question.
 * Je trouve le code de la V2 bien meilleur car il évite d'avoir a ajouter une classe pour chaque type d'alcool que nous voulons ajouter. 	
 */

public class Main {


	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Personne lambda = new Personne(0, false, 0, 0, true);
		int test = 0;
		int choix = 0;


		System.out.println("Bienvenue dans le calculateur du taux d'alcoolémie veuillez entrer votre age");
		lambda.setAge(sc.nextInt());
		System.out.println("Veuillez à présent rentrer votre poids.");
		lambda.setPoids(sc.nextInt());
		System.out.println("Êtes vous un homme? (Répondez par true ou false)");
		lambda.setSexe(sc.nextBoolean());
		System.out.println("Êtes vous un jeune permis (Répondez par true ou false)");
		lambda.setPermisJeune(sc.nextBoolean());
		System.out.println("Veuillez à présent inserer les verres que vous avez bu, il vous suffit de rentrer l'un des numéros de la liste suivante,");
		System.out.println("Vous arretez d'ajouter de l'alcool lorsque vous ecrivez 10");
		System.out.println("");
		System.out.println(java.util.Arrays.asList(Alcool.values()));

		while (test != 10) {
			test = sc.nextInt();
			switch(test) {
			case 1:
				lambda.ajouterUnVerre(Alcool.PINTE_N1);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 2:
				lambda.ajouterUnVerre(Alcool.VODKA_N2);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 3:
				lambda.ajouterUnVerre(Alcool.WHISKY_N3);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 4:
				lambda.ajouterUnVerre(Alcool.PASTIS_N4);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 5:
				lambda.ajouterUnVerre(Alcool.VIN_N5);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 6:
				lambda.ajouterUnVerre(Alcool.JAEGER_N6);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 7:
				lambda.ajouterUnVerre(Alcool.DEMI_N7);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 8:
				lambda.ajouterUnVerre(Alcool.GIN_N8);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;
			case 9:
				lambda.ajouterUnVerre(Alcool.COCKTAIL_N9);
				System.out.println(lambda.getAlcoolBu() + " dose(s) d'alcool");
				break;

			}
		}

		System.out.println("Veuillez à présent choisir entre:" + "\n" + "1. Calculer un taux d'alcoolémie" + "\n" + "2. Savoir si vous pouvez prendre le volant" + "\n" + "3. Calculer la disparition de l'alcool dans le sang en fonction du temps" + "\n");
		System.out.println("Choisissez un nombre : ");

		while (choix != 10) {
			choix = sc.nextInt();
			switch(choix) {

			case 1:
				Calcul.CalculSimpleAlcoolémie(lambda);
				System.out.println("Vous avez " + Calcul.grammeAlcool + " grammes dans le sang");
				break;

			case 2:
				Calcul.CalculPossibiliteConduire(lambda);
				break;

			case 3:
				Calcul.CalculDisparitionDeLalcool(lambda);
				break;

			case 10:
				System.out.println("le programme va se fermer.");
				System.exit(0);
				break;

			default:
				System.out.println("Veuillez à	 présent choisir entre:" + "\n" + "1. Calculer un taux d'alcolémie" + "\n" + "2. Savoir si vous pouvez prendre le volant" + "\n" + "3. Calculer la disparition de l'alcool dans le sang en fontion du temps" + "\n");
				System.out.println("Choisissez un nombre : ");
				break;
			}



		}
	}
}