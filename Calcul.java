import java.util.Scanner;

public class Calcul  {

	static float grammeAlcool;
	static float grammeAlcoolApresElim;
	static float K; //K correspond a une variable qui sert dans le calcul de l'alcool dans le sang qui est modifié en fonction du sexe.
	static Scanner sc = new Scanner(System.in);


	//Cette méthode calcule si vous avez le droit de conduire en fonction de votre permis et de votre taux d'alcool
	public static void CalculPossibiliteConduire(Personne adrien) {

		if (adrien.getSexe()) {
			K = 0.7F;
		} else {
			K = 0.6F;
		}

		if (adrien.isPermisJeune()) {
			grammeAlcool = adrien.getAlcoolBu() * 10;
			grammeAlcool = grammeAlcool / (adrien.getPoids() * K);
			if(grammeAlcool > 0.2) {
				System.out.println("Vous n'avez pas le droit de conduire!! Dormez sur place, vous avez : " + grammeAlcool + " grammes dans le sang");
			} else {
				System.out.println("Vous avez le droit de conduire. Vous avez : " + grammeAlcool + " grammes dans le sang");
			}
		} else {
			grammeAlcool = adrien.getAlcoolBu() * 10;
			grammeAlcool = grammeAlcool / (adrien.getPoids() * K);
			if(grammeAlcool > 0.5) {
				System.out.println("Vous n'avez pas le droit de conduire!! Dormez sur place, vous avez : " + grammeAlcool + " grammes dans le sang");
			} else {
				System.out.println("Vous avez le droit de conduire. Vous avez : " + grammeAlcool + " grammes dans le sang");

			}
		}

	}

	//Cette méthode calcule uniquement le nombre de gramme d'alcool que vous avez dans le sang 
	public static void CalculSimpleAlcoolémie(Personne adrien) {
		if (adrien.getSexe()) {
			K = 0.7F;
		} else {
			K = 0.6F;
		}
		grammeAlcool = adrien.getAlcoolBu() * 10;
		grammeAlcool = grammeAlcool / (adrien.getPoids() * K);

	}

	//Cette méthode permet de calculer l'alcool qui disparait
	public static void CalculDisparitionDeLalcool(Personne adrien) {
		System.out.println("Veuillez inserer en nombre d'heures approximatives la dernière fois que vous avez bu une goutte d'alcool");
		Calcul.CalculSimpleAlcoolémie(adrien);
		int heure = sc.nextInt();
		if (heure <= 0) throw new IllegalArgumentException("Veuillez inserer un chiffre positif");
		grammeAlcoolApresElim = grammeAlcool - (0.15F * heure);
		System.out.println("Vous avez éléminé: " + (grammeAlcool - grammeAlcoolApresElim) + " grammes d'alcool depuis votre dernier verre");
		System.out.println("Vous avez désormais: " + grammeAlcoolApresElim + " grammes d'alcool dans le sang");

		System.out.println("Pour rappel en France la limite est de 0.5 grammes d'alcool dans le sang, et 0.2 grammes pour les jeunes conducteurs");
	}


}

