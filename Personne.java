
public class Personne {

	private int poids;
	private boolean sexe;
	private int alcoolBu;
	private int age;
	private boolean permisJeune;


	public Personne(int poids, boolean sexe, int alcoolBu, int age, boolean permisJeune) {
		this.poids = poids;
		this.sexe = sexe;
		this.alcoolBu = alcoolBu;
		this.age = age;
		this.permisJeune = permisJeune;
	}

	public void ajouterUnVerre(Alcool beber) {
		setAlcoolBu(getAlcoolBu() + beber.getDose());
	}


	public int getPoids() {
		return this.poids;
	}


	public boolean getSexe() {
		return this.sexe;
	}


	public int getAlcoolBu() {
		return this.alcoolBu;
	}


	public int getAge() {
		return this.age;
	}


	public boolean isPermisJeune() {
		return this.permisJeune;
	}


	public void setPoids(int poids) throws IllegalArgumentException {
		if (poids < 0) {
			throw new IllegalArgumentException ("Veuillez insérer un poids positif " + poids);
		}
		this.poids = poids;
	}


	public void setSexe(boolean sexe) {
		this.sexe = sexe;
	}


	public void setAlcoolBu(int alcoolBu) {
		this.alcoolBu = alcoolBu;
	}


	public void setAge(int age) throws IllegalArgumentException {
		if (age < 18) {
			System.out.println("Vous êtes trop jeune");
			System.exit(0);
		}
		this.age = age;
	}


	public void setPermisJeune(boolean permisJeune) {
		this.permisJeune = permisJeune;
	}





}
