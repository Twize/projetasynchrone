
public enum Alcool {

	PINTE_N1("Double bière", 2),
	VODKA_N2("Cocktail à base de vodka", 2),
	WHISKY_N3("Melange fait avec du whisky", 2),
	PASTIS_N4("Pastis mélangé à l'eau", 2),
	VIN_N5("Bouteille de vin", 3),
	JAEGER_N6("Alcool à base de plantes", 2),
	DEMI_N7("Biere", 1),
	GIN_N8("Verre de gin à l'anglaise", 2),
	COCKTAIL_N9("Différents cocktails", 2); 

	protected String désignation = "";
	protected int dose = 0; 

	Alcool(String désignation, int dose) {
		this.désignation = désignation;
		this.dose = dose;
	}

	public String getDésignation() {
		return this.désignation;
	}

	public int getDose() {
		return this.dose;
	}

	public void setDésignation(String désignation) {
		this.désignation = désignation;
	}

	public void setDose(int dose) {
		this.dose = dose;
	}

}
